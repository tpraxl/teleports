pragma Singleton
import QtQuick 2.6

QtObject {

    readonly property color backgroundColor: "#ffffff"
    readonly property color tileColor: "#333333"
    readonly property color incomingMessageColor: "#eeedeb"
    readonly property color unsentMessageColor: "#cccccc"
    readonly property color readMessageColor: "#8bd195"
    readonly property color sentMessageColor: "#75d3f5"
    readonly property color errorMessageColor: "#f15a6b"
    readonly property color textColor: "#181818"
    readonly property color textReplyColor: "#3d3d3d"
    readonly property color codeColor: "#666666"
    readonly property color linkPreviewColor: "#666666"
    readonly property color linkActionColor: "#ffffff"
    readonly property color messageActionColor: "#888888"
    readonly property color messageActionBackground: "#b5b5b5"
    readonly property color separatorColor: "#d5d5d5"
    readonly property color linkColor: "#006a97"
    readonly property color tertiaryTextColor: "#666666"

}
