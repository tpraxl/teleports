# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-13 19:44+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#. TRANSLATORS: Application name.
#: ../push/pushhelper.cpp:122 ../app/qml/pages/ChatListPage.qml:20
#: ../app/qml/pages/AboutPage.qml:65 teleports.desktop.in.h:1
msgid "TELEports"
msgstr ""

#: ../push/pushhelper.cpp:132
msgid "sent you a message"
msgstr ""

#: ../push/pushhelper.cpp:136
msgid "sent you a photo"
msgstr ""

#: ../push/pushhelper.cpp:140
msgid "sent you a sticker"
msgstr ""

#: ../push/pushhelper.cpp:144
msgid "sent you a video"
msgstr ""

#: ../push/pushhelper.cpp:148
msgid "sent you a document"
msgstr ""

#: ../push/pushhelper.cpp:152
msgid "sent you an audio message"
msgstr ""

#: ../push/pushhelper.cpp:156
msgid "sent you a voice message"
msgstr ""

#: ../push/pushhelper.cpp:160
msgid "shared a contact with you"
msgstr ""

#: ../push/pushhelper.cpp:164
msgid "sent you a map"
msgstr ""

#: ../push/pushhelper.cpp:169
msgid "%1: %2"
msgstr ""

#: ../push/pushhelper.cpp:174
msgid "%1 sent a message to the group"
msgstr ""

#: ../push/pushhelper.cpp:179
msgid "%1 sent a photo to the group"
msgstr ""

#: ../push/pushhelper.cpp:184
msgid "%1 sent a sticker to the group"
msgstr ""

#: ../push/pushhelper.cpp:189
msgid "%1 sent a video to the group"
msgstr ""

#: ../push/pushhelper.cpp:194
msgid "%1 sent a document to the group"
msgstr ""

#: ../push/pushhelper.cpp:199
msgid "%1 sent a voice message to the group"
msgstr ""

#: ../push/pushhelper.cpp:204
msgid "%1 sent a contact to the group"
msgstr ""

#: ../push/pushhelper.cpp:209
msgid "%1 sent a map to the group"
msgstr ""

#: ../push/pushhelper.cpp:214 ../push/pushhelper.cpp:235
msgid "%1 invited you to the group"
msgstr ""

#: ../push/pushhelper.cpp:219
msgid "%1 changed group name"
msgstr ""

#: ../push/pushhelper.cpp:224
msgid "%1 changed group photo"
msgstr ""

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:230
msgid "%1 invited %2"
msgstr ""

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../push/pushhelper.cpp:241
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
msgid "%1 removed %2"
msgstr ""

#: ../push/pushhelper.cpp:246
msgid "%1 removed you from the group"
msgstr ""

#: ../push/pushhelper.cpp:251
msgid "%1 has left the group"
msgstr ""

#: ../push/pushhelper.cpp:256
msgid "%1 has returned to the group"
msgstr ""

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:261
msgid "@ %1"
msgstr ""

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:263
msgid "%1 has checked-in"
msgstr ""

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:269
msgid "%1 joined Telegram!"
msgstr ""

#: ../push/pushhelper.cpp:274 ../push/pushhelper.cpp:280
msgid "New login from unrecognized device"
msgstr ""

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:279
msgid "%1 @ %2"
msgstr ""

#: ../push/pushhelper.cpp:284
msgid "updated profile photo"
msgstr ""

#: ../push/pushhelper.cpp:289 ../push/pushhelper.cpp:294
#: ../push/pushhelper.cpp:299
msgid "You have a new message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:328
msgid "Sticker"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:332
msgid "Phone call"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:336
msgid "sent an audio message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:340
msgid "sent a photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:349
msgid "Location"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:353
msgid "sent a video"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:357
msgid "sent a video note"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:361
msgid "sent a voice note"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:365
msgid "joined the group"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:369
msgid "changed the chat photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:373
msgid "changed the chat title"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:377
msgid "joined by invite link"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:381
msgid "removed a member"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:385
msgid "deleted the chat photo"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:390
msgid "upgraded to supergroup"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:394
msgid "message TTL has been changed"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:399
msgid "created this group"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:408
#: ../app/qml/delegates/MessageUnsupported.qml:4
msgid "Unsupported message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:411
msgid "Unimplemented: %1"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessage.cpp:416
msgid "Me"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessagecontent.cpp:6
msgid "Message"
msgstr ""

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:194
msgid "Unread Messages"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:25
msgid "Last seen one month ago"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:35
msgid "Last seen one week ago"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:50
msgid "Last seen "
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:50
msgid "dd.MM.yy hh:mm"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:72
#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:31
msgid "Online"
msgstr ""

#: ../libs/qtdlib/user/qtduserstatus.cpp:89
msgid "Seen recently"
msgstr ""

#: ../libs/qtdlib/chat/qtdchat.cpp:71
msgid "Saved Messages"
msgstr ""

#. TRANSLATORS: Used in attach menu, when sending a photo to the conversation.
#: ../app/qml/components/AttachPanel.qml:55
msgid "Image"
msgstr ""

#. TRANSLATORS: Used in attach menu, when sending a document to the conversation.
#: ../app/qml/components/AttachPanel.qml:64
msgid "File"
msgstr ""

#: ../app/qml/components/UserProfile.qml:77
#: ../app/qml/components/UserProfile.qml:101
msgid "not available"
msgstr ""

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:29
msgid "Choose a country"
msgstr ""

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr ""

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr ""

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:30
msgid "Cancel"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:21
msgid "Are you sure you want to clear the history?"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:22
#: ../app/qml/pages/ChatListPage.qml:81
msgid "Clear history"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:31
msgid "Are you sure you want to leave this chat?"
msgstr ""

#: ../app/qml/middleware/ChatMiddleware.qml:32
msgid "Leave"
msgstr ""

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:77
msgid "Auth code not expected right now"
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:83
msgid "Oops! Internal error."
msgstr ""

#: ../app/qml/stores/AuthStateStore.qml:89
msgid "Incorrect auth code length."
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:19
msgid "Enter Code"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:45
msgid "First Name"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:53
msgid "Last Name"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:60
msgid "Code"
msgstr ""

#: ../app/qml/pages/WaitCodePage.qml:75
msgid "We've send a code via telegram to your device. Please enter it here."
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:19
msgid "Select destination or cancel..."
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:40 ../app/qml/pages/SettingsPage.qml:23
msgid "Settings"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:76
msgid "Leave chat"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:92
msgid "Info"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:302
msgid "Do you want to forward the selected messages to %1?"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:304
#: ../app/qml/delegates/MessageBubbleItem.qml:82
msgid "Forward"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:318
msgid "Enter optional message..."
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:13
msgid "Enter Phone Number"
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:47
#: ../app/qml/pages/WaitPhoneNumberPage.qml:57
msgid "Phone number"
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:81
msgid "Please confirm your country code and enter your phone number."
msgstr ""

#: ../app/qml/pages/WaitPhoneNumberPage.qml:85
#: ../app/qml/pages/WaitPasswordPage.qml:48
msgid "Next..."
msgstr ""

#: ../app/qml/pages/GroupDetailsPage.qml:21
msgid "Group Details"
msgstr ""

#: ../app/qml/pages/GroupDetailsPage.qml:32 ../app/qml/pages/AboutPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:47
#: ../app/qml/pages/SettingsPage.qml:34 ../app/qml/pages/UserProfilePage.qml:30
msgid "Back"
msgstr ""

#: ../app/qml/pages/GroupDetailsPage.qml:96
msgid "Members: %1"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/SettingsPage.qml:45
msgid "About"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:70
msgid "Version %1"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:92
msgid "Get the source"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:93
msgid "Report issues"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:94
msgid "Help translate"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Offline"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:25
#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Connecting"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting To Proxy"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:30
msgid "Updating"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:36
msgid "Connectivity"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:75
msgid "Telegram connectivity status:"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:82
msgid "Ubuntu Touch connectivity status:"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith limited"
msgstr ""

#: ../app/qml/pages/ConnectivityPage.qml:89
msgid "Ubuntu Touch bandwith not limited"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:82
msgid "Night mode"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:102 ../app/qml/pages/SettingsPage.qml:147
msgid "Logout"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:115
msgid "Delete account"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:128
msgid "Connectivity status"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:145
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:155
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"send using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:157
#: ../app/qml/delegates/MessageBubbleItem.qml:33
#: ../app/qml/delegates/MessageBubbleItem.qml:47
msgid "Delete"
msgstr ""

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr ""

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr ""

#: ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:28
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] ""
msgstr[1] ""

#: ../app/qml/pages/MessageListPage.qml:114
msgid "Telegram"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:268
#: ../app/qml/delegates/MessageBubbleItem.qml:62
msgid "Edit"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:268
#: ../app/qml/delegates/MessageBubbleItem.qml:70
msgid "Reply"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:342
msgid "Type a message..."
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:453
msgid "Requesting location from OS..."
msgstr ""

#: ../app/qml/pages/UserProfilePage.qml:19
msgid "Profile"
msgstr ""

#: ../app/qml/pages/WaitPasswordPage.qml:13
msgid "Enter Password"
msgstr ""

#: ../app/qml/pages/WaitPasswordPage.qml:32
msgid "Password"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:11
msgid "%1 joined the group"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:12
msgid "%1 added %2"
msgstr ""

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:49
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] ""
msgstr[1] ""

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr ""

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:44
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:45
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:56
msgid "Copy"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:76
msgid "Sticker Pack info"
msgstr ""

#: ../app/qml/delegates/MessageBubbleItem.qml:212
msgid "Edited"
msgstr ""

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr ""

#: ../app/qml/delegates/MessageContentBase.qml:35
msgid "Forwarded from "
msgstr ""

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr ""

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:51
msgid "Call Declined"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:53
msgid "Call Disconnected"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:56
msgid "Call Ended"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Cancelled Call"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:58
msgid "Missed Call"
msgstr ""

#: ../app/qml/delegates/MessageContentCall.qml:71
msgid "Duration: %1:%2"
msgstr ""

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called << %1 >> created"
msgstr ""

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called << %2 >>"
msgstr ""

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "dd MMMM"
msgstr ""

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr ""

#: ../app/qml/delegates/MessageContentVoiceNote.qml:67
msgid "Voice note"
msgstr ""
